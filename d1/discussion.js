// [SECTION] Comparison Query Operator

	//$gt/$gte operator
	/* Allows us to find documents thathave field number values greater than or equal to a specified value
		Syntax:
		db.collectionName.find ({field: {$gt:value}});
	*/


		db.users.find({age: {$gt:50}});
		db.users.find({age: {$gte:50}});


	//$lt/$lte operator

	/* Allows us to find documents thathave field number values less than or equal to a specified value
		Syntax:
		db.collectionName.find ({field: {$lt:value}});
	*/

		db.users.find({age: {$lt:50}});
		db.users.find({age: {$lte:50}});


	//$ne operator
		
	/*	Allows us to find documents that have field number not equal to a specified value
		
		Syntax:
		db.collectionName.find({field: {$ne:value}});
	*/

		db.users.find({age: {$ne:82}});


	//$in operator
	/* 	
		Allows us to find document with speicific cirteria using different values
		Syntax:
		db.collectionName.find({field: {$in:value}});
	*/

		db.users.find({lastName: {$in: ["Hawking", "Doe" ] } });
		db.users.find({courses: {$in: ["HTML", "React" ] } });


//[SECTION] Logical Query Operators

	// $or operator
	/* 
		Allows us to find documents that match a single criteria from multiple provided search criteria
		Syntax:
		db.collectionName.find ({$or: [{field: value}, {field: value}]});
	*/

		db.users.find ({$or: [{firstName: "Neil"}, {age: 25}]});

		db.users.find ({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});


	//$and operator
	/*
		Allows us to find matching multiple criteria in a single field
		Syntax:
			db.collectionName.find ({$and: [{fieldA: valueA}, {fieldB: valueB}]})
	*/
	
		db.users.find ({$and: [{age: {$ne:82}}, {age: {$ne:76}}]})


	//[SECTION] Field Projection

	//Inclusion
	/* 
		Allows us to include a specific field /s when retrieving documents.
		The value provided is 1 to denote that the field is being included
		Syntax:
			db.collectionName.find({criteria}, {field: 1})
	*/

		db.users.find(
			{
				firstName: "Jane",
			},
			{
				firstName: 1, 
				lastName: 1,
				contact:1
			}
		);



//exclusion
		/* 
		Allows us to exclude a specific field /s when retrieving documents.
		The value provided is 0 to denote that the field is being excluded
		Syntax:
			db.collectionName.find({criteria}, {field: 0})
	*/
		db.users.find(
		{
			firstName: "Jane"
		},
		{
			contact: 0,
			department: 0,
			
		}
		);

		// suppresing the ID field

		/*
			Allows us to exclude the "_id" when retrieving documents
			when using the field projection, field inlcusion and exclusion cannot be used at the same time.
			The only exception is when excluding the "_id".

		db.users.find(
			{
				firstName: "Jane",
			},
			{
				firstName: 1, 
				lastName: 1,
				contact:1,
				_id=0
			}
		);